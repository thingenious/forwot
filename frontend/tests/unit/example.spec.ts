import { shallowMount } from "@vue/test-utils";
import RouterComponent from "@/components/RouterComponent.vue";

describe("RouterComponent.vue", () => {
  it("renders slot.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(RouterComponent, {
      slots: {
        default: `<div>${msg}</div>`,
      },
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
