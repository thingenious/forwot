import axios, { AxiosResponse } from "axios";

axios.defaults.timeout = 10 * 60 * 1000; // 10 minutes timeout

const domainsQuery = (domains: string[]): string => {
  let _query = "";
  domains.forEach((domain, index) => {
    _query += index == 0 ? "?" : "&";
    _query += `domains=${domain}`;
  });
  return _query;
};

export const api = {
  async getDomains(): Promise<AxiosResponse> {
    return axios.get("/forwot/api/ontologies/domains");
  },
  async getOntologies(): Promise<AxiosResponse> {
    return axios.get("/forwot/api/ontologies");
  },
  async getWordcloud(domains: string[]): Promise<AxiosResponse> {
    const query = domainsQuery(domains);
    return axios.get(`/forwot/api/wordcloud${query}`);
  },
  async getStats(domains: string[]): Promise<AxiosResponse> {
    const query = domainsQuery(domains);
    return axios.get(`/forwot/api/stats${query}`);
  },
  async search(
    domain: string,
    term: string,
    simple: boolean
  ): Promise<AxiosResponse> {
    const query = `?ontology_domain=${domain}&search_term=${term}`;
    const path = simple ? "/search-term" : "/search-similarity";
    return axios.get(`/forwot/api${path}${query}`);
  },
};
