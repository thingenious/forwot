import { MainState } from "@/interfaces";

export interface State {
  main: MainState;
}
