import { getStoreAccessors } from "typesafe-vuex";
import { ActionContext } from "vuex";
import { api } from "@/api";

import {
  commitAddNotification,
  commitRemoveNotification,
  commitSetDomains,
  commitSetOntologies,
} from "./mutations";
import { State } from "./state";
import { MainState, AppNotification } from "@/interfaces";
import { StatsResponse } from "@/interfaces/StatsResponse";

type MainContext = ActionContext<MainState, State>;

export const actions = {
  async removeNotification(
    context: MainContext,
    payload: { notification: AppNotification; timeout: number }
  ): Promise<boolean> {
    return new Promise((resolve) => {
      setTimeout(() => {
        commitRemoveNotification(context, payload.notification);
        resolve(true);
      }, payload.timeout);
    });
  },
  async getDomains(context: MainContext): Promise<void> {
    try {
      const response = await api.getDomains();
      if (response.data) {
        commitSetDomains(context, response.data);
      }
    } catch (error) {
      console.warn(error);
    }
  },
  async getOntologies(context: MainContext): Promise<void> {
    try {
      const loadingNotification = {
        content: "Fetching Ontologies...",
        showProgress: true,
      };
      commitAddNotification(context, loadingNotification);
      const response = await api.getOntologies();
      if (response.data) {
        setTimeout(() => {
          commitSetOntologies(context, response.data);
          commitRemoveNotification(context, loadingNotification);
          commitAddNotification(context, {
            content: "Ontologies successfully fetched",
            color: "success",
          });
        }, 1000);
      }
    } catch (error) {
      console.warn(error);
    }
  },
  async getStats(
    context: MainContext,
    payload: { domains: string[] }
  ): Promise<StatsResponse | null> {
    try {
      const loadingNotification = {
        content: "Fetching Stats...",
        showProgress: true,
      };
      commitAddNotification(context, loadingNotification);
      const response = await api.getStats(payload.domains);
      return new Promise<StatsResponse | null>((resolve) => {
        if (response.data) {
          commitRemoveNotification(context, loadingNotification);
          commitAddNotification(context, {
            content: "Stats successfully fetched",
            color: "success",
          });
          resolve(response.data);
        } else {
          resolve(null);
        }
      });
    } catch (error) {
      console.warn(error);
      return new Promise<StatsResponse | null>((resolve) => {
        resolve(null);
      });
    }
  },
};

const { dispatch } = getStoreAccessors<MainState, State>("");
export const dispatchRemoveNotification = dispatch(actions.removeNotification);
export const dispatchGetOntologies = dispatch(actions.getOntologies);
export const dispatchGetStats = dispatch(actions.getStats);
export const dispatchGetDomains = dispatch(actions.getDomains);
