export interface StatsResponse {
  frequencies: { [key: string]: number };
  colors: { [key: string]: string };
  palette: string[];
  common_color: string;
  domains: string[];
}
