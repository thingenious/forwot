import Vue from "vue";
import Router from "vue-router";
const RouterComponent = () =>
  import(/* webpackPrefetch: true */ "@/components/RouterComponent.vue");

Vue.use(Router);

export default new Router({
  mode: "history",
  base: "/forwot",
  routes: [
    {
      path: "/",
      component: RouterComponent,
      redirect: "/home",
      children: [
        {
          path: "/home",
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () =>
            import(
              /* webpackPrefetch: true */ /* webpackChunkName: "home" */ "./views/Home.vue"
            ),
        },
        {
          path: "/ontologies",
          component: () =>
            import(
              /* webpackPrefetch: true */ /* webpackChunkName: "ontologies" */ "./views/Ontologies.vue"
            ),
        },
        {
          path: "/search",
          component: () =>
            import(
              /* webpackPrefetch: true */ /* webpackChunkName: "search" */ "./views/Search.vue"
            ),
        },
        {
          path: "/wordcloud",
          component: () =>
            import(
              /* webpackPrefetch: true */ /* webpackChunkName: "wordcloud" */ "./views/Stats.vue"
            ),
        },
      ],
    },
    {
      path: "/*",
      redirect: "/home",
    },
  ],
});
