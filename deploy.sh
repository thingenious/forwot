#!/bin/sh
# shellcheck disable=SC2086
# download the manifests dir
if [ -d .forwot-manifests ]; then
    rm -rf .forwot-manifests
fi
git clone https://gitlab.com/thingenious.io/forwot.git forwot_tmp && mv forwot_tmp/manifests .forwot-manifests && rm -rf forwot_tmp
# work on a unique namespace (also needed for dns resolving)
kubectl apply -f .forwot-manifests/01-namespace.yaml
################################################################################
# pods & services
###################################### DB ######################################
# persistent volume claim for db data
kubectl apply -f .forwot-manifests/02-db-persistentvolumeclaim.yaml
# db deployment
kubectl apply -f .forwot-manifests/03-db-deployment.yaml
# expose db port (db service)
kubectl apply -f .forwot-manifests/04-db-service.yaml
################################## Torchserve ##################################
# deployment
kubectl apply -f .forwot-manifests/05-torchserve-deployment.yaml
# expose ports (service)
kubectl apply -f .forwot-manifests/06-torchserve-service.yaml
################################## Backend #####################################
# deployment
kubectl apply -f .forwot-manifests/07-backend-deployment.yaml
# expose ports (service)
kubectl apply -f .forwot-manifests/08-backend-service.yaml
################################## Frontend ####################################
# deployment
kubectl apply -f .forwot-manifests/09-frontend-deployment.yaml
# expose ports (service)
kubectl apply -f .forwot-manifests/10-frontend-service.yaml
################################## CronJob #####################################
kubectl apply -f .forwot-manifests/11-cronjob.yaml
################################################################################
# remove the manifests folder
rm -rf .forwot-manifests
