from typing import Optional, final, List, Union, Dict, Any

import pandas as pd  # noqa
from sqlalchemy.ext.asyncio import AsyncSession

from app.internal.models import Ontology, OntologyEntity  # noqa
from app.internal.transaction.ontology_entity_transaction import (
    OntologyEntityTransaction,
)  # noqa
from app.internal.service.preprocessor import Preprocessor  # noqa


@final
class OntologyEntityService:

    @staticmethod
    async def _preprocess(
        session: AsyncSession, ontology_entity_dataframe: pd.DataFrame
    ) -> None:
        """preprocess ontology classes in dataframe and store results in new preprocessed column."""
        ontology_entity_dataframe["preprocessed"] = ontology_entity_dataframe[
            "ontology_class"
        ].apply(Preprocessor.preprocess)
        for series in ontology_entity_dataframe[["id", "preprocessed"]].values:
            ontology_entity_id = series[0]
            preprocessed_value = series[1]
            await OntologyEntityTransaction.update_ontology_entity_preprocessed_property(
                session=session,
                ontology_entity_id=ontology_entity_id,
                preprocessed_value=preprocessed_value,
            )
        await session.commit()

    @staticmethod
    async def get_list_of_ontologies_entities_dataframes(
        session, domain_list, max_depth=None
    ):
        # type: (AsyncSession, List[str], Optional[int]) -> List[pd.DataFrame]
        dataframes = []
        for domain in domain_list:
            entities = await OntologyEntityTransaction.get_ontology_entities_by_domain_and_active(
                session, domain, max_depth=max_depth
            )
            entities_dict = OntologyEntity.to_dict(entities)
            dataframe = pd.DataFrame(entities_dict)
            # check if any (or should we use all() instead of any()?) "preprocessed" field is NaN
            if dataframe["preprocessed"].isnull().any():
                print("not yet preprocessed...")
                await OntologyEntityService._preprocess(
                    session=session, ontology_entity_dataframe=dataframe
                )
            dataframes.append(dataframe)
        return dataframes
