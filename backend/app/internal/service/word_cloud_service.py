import base64
import io
from collections import Counter
from typing import final, List, Any, Dict, Optional, Union, Set, Tuple, Callable

import numpy as np  # noqa
import pandas as pd  # noqa
from sqlalchemy.ext.asyncio import AsyncSession
from PIL.Image import Image  # noqa
from wordcloud import WordCloud  # noqa
from sklearn.cluster import AgglomerativeClustering  # noqa

from app.internal.service.embeddings import EmbeddingsService  # noqa
from app.internal.service.ontology_entity_service import OntologyEntityService  # noqa
from app.internal.service.grouped_color_func import (  # noqa
    COMMON_WORDS_COLOR,
    DEFAULT_COLORS,
    GroupedColorFunc,
)


@final
class WordCloudService:
    DEFAULT_MAX_WORDS = 100
    CLUSTER_MAPPINGS = {
        "Transportation": 80,
        "Weather": 50,
        "EnergyFOI": 80,
        "Health": 85,
        "BuildingAutomation": 90,
    }

    @staticmethod
    def _domain_clusters(
        domains,  # type: List[str]
        domain_dfs,  # type: List[pd.DataFrame]
        domain_embeddings,  # type: List[np.ndarray]
    ):
        # type: (...) -> None
        """Add a `clusters` column for each domain's dataframe."""
        if not len(domain_dfs) == len(domain_embeddings):
            raise ValueError("Invalid number of domain dataframes / domain_embeddings")
        for df_index, domain_df in enumerate(domain_dfs):
            n_clusters = WordCloudService.CLUSTER_MAPPINGS.get(domains[df_index], 50)
            hierarchical = AgglomerativeClustering(
                distance_threshold=None,
                n_clusters=n_clusters,
                affinity="euclidean",
                linkage="ward",
            )
            domain_df["clusters"] = hierarchical.fit_predict(
                domain_embeddings[df_index]
            )

    @staticmethod
    def _wc_color_func(per_domain_terms, common_terms, **kwargs):
        # type: (List[List[str]], List[str], Any) ->GroupedColorFunc
        count = len(per_domain_terms)
        wc_colors = list(DEFAULT_COLORS[:]) + [COMMON_WORDS_COLOR]
        colors: List[str] = kwargs.get("colors", wc_colors)
        if len(colors) != count + 1:
            colors = wc_colors
        common_words_color = colors.pop()
        color_to_words: Dict[str, Union[List[str], Set[str]]] = {}
        for index in range(count):
            # red, green, yellow ...
            color = colors[index % len(colors)]
            color_to_words[color] = set(per_domain_terms[index])
        return GroupedColorFunc(
            color_to_words=color_to_words,
            common_words=common_terms,
            default_color=common_words_color,
        )

    @staticmethod
    def _domain_cluster_texts(domain_df, n_clusters, as_words=True):
        # type: (pd.DataFrame, int, Optional[bool]) -> List[str]
        """Get text of a domain's clusters.

        All the terms/words we have, that belong to the same cluster
        could be:    ["single", "words", "only"]
        or:          ["not", "only", "single words""]
        """
        terms = []  # type: List[str]
        for cluster in range(n_clusters):
            domain_terms = domain_df["preprocessed"][
                domain_df["clusters"] == cluster
            ]  # type: pd.Series
            if as_words:
                text = ""
                for term in domain_terms:
                    text += term + " "
                terms.append(text)
            else:
                terms.extend(domain_terms.tolist())
        return terms

    @staticmethod
    def _domain_top_term(domain_cluster_texts, as_words=True):
        # type: (List[str], Optional[bool]) -> List[pd.Series]
        """Get the domain's most common  term/word."""
        cloud = []  # type: List[pd.Series]
        for domain_cluster_text in domain_cluster_texts:
            temp_df = pd.DataFrame(
                {
                    "words": domain_cluster_text.split(" ")
                    if as_words
                    else [domain_cluster_text]
                }
            )
            _top = temp_df["words"].value_counts().nlargest(1)  # type: pd.Series
            cloud.append(_top)
        return cloud

    @staticmethod
    def _domain_text(domain_cluster_texts, n_clusters, as_words=True):
        # type: (List[str], int, Optional[bool]) -> str
        """Get the text to use for the stats/wordcloud for a specific domain."""
        domain_text = ""  # type: str
        cloud = WordCloudService._domain_top_term(
            domain_cluster_texts, as_words=as_words
        )
        for cluster_index in range(n_clusters):
            try:
                for _ in range(cloud[cluster_index][0]):
                    domain_text += cloud[cluster_index].index[0] + " "
            except (IndexError, ValueError, Exception) as error:  # pragma: nocover
                print(error)
        return domain_text.strip()

    @staticmethod
    def _terms(
        domain_dfs,  # type: List[pd.DataFrame]
        as_words=True,  # type: Optional[bool]
    ):
        # type: (...) -> Tuple[List[List[str]], List[str], List[str]]
        """Get per domain terms and terms that are common in all domains."""
        per_domain = []  # type: List[List[str]]
        domains = []  # type: List[str]
        for domain_df in domain_dfs:
            domain_name = domain_df["domain"].iloc[0]
            domains.append(domain_name)
            n_clusters = len(np.unique(domain_df["clusters"]))
            domain_cluster_texts = WordCloudService._domain_cluster_texts(
                domain_df=domain_df, n_clusters=n_clusters, as_words=as_words
            )  # type : List[str]
            if as_words:
                _domain_text = WordCloudService._domain_text(
                    domain_cluster_texts=domain_cluster_texts,
                    n_clusters=n_clusters,
                    as_words=as_words,
                )
                _domain_terms = _domain_text.split()
            else:
                _domain_terms = domain_cluster_texts
            per_domain.append(_domain_terms)
        return (
            per_domain,
            list(set.intersection(*[set(terms) for terms in per_domain])),
            domains,
        )

    @staticmethod
    def _colors_and_frequencies(per_domain_terms, color_func):
        # type: (List[List[str]], Callable) ->Tuple[Dict[str, int],Dict[str, str]]
        """Calculate term frequencies, in order to produce a wordcloud."""
        all_terms = []
        term_colors = {}  # type: Dict[str,str]
        for terms in per_domain_terms:
            for term in terms:
                all_terms.append(term)
                term_colors[term] = color_func(term)

        counts = Counter(all_terms)
        # also sort them (most common first, to get max weight)
        frequencies = {item[0]: item[1] for item in counts.most_common()}
        return frequencies, term_colors

    @staticmethod
    def _frequencies(per_domain_terms):
        # type: (List[List[str]]) ->Dict[str, int]
        """Calculate term frequencies, in order to produce a wordcloud."""
        all_terms = [
            term for domain_terms in per_domain_terms for term in domain_terms
        ]  # type: List[str]
        frequencies = Counter(all_terms)
        # also sort them (most common first, to get max weight)
        return {item[0]: item[1] for item in frequencies.most_common()}

    @staticmethod
    def wordcloud_stats(
        domain_dfs,  # type: List[pd.DataFrame]
        **kwargs,  # type: Any
    ):
        # type: (...) -> Dict[str, Any]
        """Get frequencies and commons words between the domains."""
        terms_as_words = kwargs.get("terms_as_words", True)
        per_domain_terms, common_terms, domain_names = WordCloudService._terms(
            domain_dfs=domain_dfs, as_words=terms_as_words
        )
        recolor_func = WordCloudService._wc_color_func(
            per_domain_terms=per_domain_terms, common_terms=common_terms, **kwargs
        )
        frequencies, colors = WordCloudService._colors_and_frequencies(
            per_domain_terms=per_domain_terms, color_func=recolor_func
        )
        return {
            "frequencies": frequencies,
            "domains": domain_names,
            "common": common_terms,
            "per_domain": per_domain_terms,
            "colors": colors,
            "common_color": COMMON_WORDS_COLOR,
            "palette": DEFAULT_COLORS,
        }

    @staticmethod
    def wordcloud_image(
        domain_dfs,  # type: List[pd.DataFrame]
        **kwargs,  # type: Any
    ):
        # type: (...) -> Image
        """Generate a wordcloud with words from the domains."""
        per_domain_terms, common_terms, _ = WordCloudService._terms(
            domain_dfs=domain_dfs,
            as_words=kwargs.get("terms_as_words", True),
        )
        wcl = WordCloud(
            width=kwargs.get("width", 1024),
            height=kwargs.get("height", 768),
            max_font_size=kwargs.get("max_font_size", 50),
            max_words=kwargs.get("max_words", WordCloudService.DEFAULT_MAX_WORDS),
            background_color=kwargs.get("background_color", "white"),
            collocations=kwargs.get("collocations", False),
        )
        frequencies = WordCloudService._frequencies(per_domain_terms=per_domain_terms)
        wcl.generate_from_frequencies(
            frequencies=frequencies, max_font_size=kwargs.get("max_font_size", 50)
        )
        if len(domain_dfs) == 1:
            return wcl.to_image()
        recolor_func = WordCloudService._wc_color_func(
            per_domain_terms=per_domain_terms, common_terms=common_terms, **kwargs
        )
        wcl.recolor(color_func=recolor_func)
        return wcl.to_image()

    @staticmethod
    async def create_wordcloud(session, domains, depth=None):
        # type:(AsyncSession, List[str], int) ->Optional[bytes]
        # step1: Fetch entities
        domain_dfs = (
            await OntologyEntityService.get_list_of_ontologies_entities_dataframes(
                session, domains, max_depth=depth
            )
        )
        # step 2: get the embeddings for each domain
        domain_embeddings = await EmbeddingsService.get_embedding_by_domains(
            domains=domains,
            category="preprocessed",
            domain_dataframes=domain_dfs,
            depth=depth,
        )
        if domain_embeddings:
            WordCloudService._domain_clusters(
                domains=domains,
                domain_dfs=domain_dfs,
                domain_embeddings=domain_embeddings,
            )
            image_wordcloud = WordCloudService.wordcloud_image(
                domain_dfs=domain_dfs,
            )
            buffered = io.BytesIO()
            image_wordcloud.save(buffered, format="JPEG")
            img_str = base64.b64encode(buffered.getvalue())
            return img_str
        return None

    @staticmethod
    async def get_wordcloud_stats(session, domains, depth=None):  # noqa, TODO
        # type:(AsyncSession, List[str], Optional[int]) ->Dict[str,Any]
        # step1: Fetch entities
        domain_dfs = (
            await OntologyEntityService.get_list_of_ontologies_entities_dataframes(
                session,
                domains,
                max_depth=depth,
            )
        )
        domain_embeddings = await EmbeddingsService.get_embedding_by_domains(
            domains=domains,
            domain_dataframes=domain_dfs,
            category="preprocessed",
            depth=depth,
        )
        WordCloudService._domain_clusters(
            domains=domains, domain_dfs=domain_dfs, domain_embeddings=domain_embeddings
        )
        return WordCloudService.wordcloud_stats(
            domain_dfs=domain_dfs,
        )
