"""-*- coding: utf-8 -*-."""
import re
from typing import List

import nltk  # noqa
from nltk.tokenize import word_tokenize  # noqa
from nltk.corpus import stopwords  # noqa

try:
    nltk.download("stopwords")
    nltk.download("punkt")
except (FileExistsError, Exception):
    pass

STOPWORDS = stopwords.words("english")
STOPWORDS.append("due")
STOPWORDS.append("dv")
STOPWORDS.append("wm")
STOPWORDS.append("wms")
STOPWORDS.append("yes")
STOPWORDS.append("obs")
STOPWORDS.append("italian")
STOPWORDS.append("spanish")
STOPWORDS.append("arabic")
STOPWORDS.append("french")
STOPWORDS.append("xsd")


class Preprocessor:

    # split 2 consecutive capital letters
    @staticmethod
    def do_upper_split(word):
        # type: (str) -> bool
        for letter in word:
            if letter.isupper():
                if re.match(r"[A-Z]{2,}", word):
                    return False
                else:
                    return True
            else:
                return False
        return False

    # remove single chars that are repeated a lot of times
    # making clusters due to this fact. E.g., the char v from words v_canal, v_route etc.
    @staticmethod
    def join_upper(word_list):
        # type: (List[str]) -> List
        times = len(word_list)
        for i in range(times):
            if len(word_list[i]) == 1:
                word_list[i] = ""
        return word_list

    # not the best preprocessing code since it eliminates some words.
    # E.g. CO2 is completely removed, so we could add a white list
    @staticmethod
    def preprocess(term):
        # type: (str) -> str
        no_uri_classes = term
        _no_uri_classes_chars = " ".join(re.findall("[a-zA-Z]+", no_uri_classes))
        if Preprocessor.do_upper_split(_no_uri_classes_chars):
            no_uri_classes_chars = re.findall("[A-Z][^A-Z]*", _no_uri_classes_chars)
        else:
            no_uri_classes_chars = _no_uri_classes_chars.split()
        lower_split_words = " ".join(
            [sw.lower() for sw in Preprocessor.join_upper(no_uri_classes_chars)]
        )
        lower_split_words = re.sub(" +", " ", lower_split_words)

        text_tokens = word_tokenize(lower_split_words)
        tokens_without_sw = [
            word for word in text_tokens if len(word) > 1 and word not in STOPWORDS
        ]

        return " ".join(tokens_without_sw)
