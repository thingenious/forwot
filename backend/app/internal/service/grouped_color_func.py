"""-*- coding: utf-8 -*-."""
from typing import Callable, Dict, List, Optional, Set, Union, Any, final  # noqa
from wordcloud import get_single_color_func  # noqa

COMMON_WORDS_COLOR = "blue"

# >4: from bokeh.palettes/d3.scale.Category20
DEFAULT_COLORS = (
    "red",
    "green",
    "orange",
    "grey",
    "#1f77b4",
    "#ff7f0e",
    "#2ca02c",
    "#d62728",
    "#9467bd",
    "#8c564b",
    "#e377c2",
    "#7f7f7f",
    "#bcbd22",
    "#17becf",
    "#aec7e8",
    "#ff7f0e",
    "#ffbb78",
    "#98df8a",
    "#ff9896",
    "#c5b0d5",
    "#c49c94",
    "#f7b6d2",
    "#c7c7c7",
    "#dbdb8d",
    "#9edae5",
)


@final
class GroupedColorFunc:
    """Custom color function object for different color per group of words.

    It assigns DIFFERENT SHADES of specified colors to certain words based
    on the color to words mapping. Uses wordcloud.get_single_color_func
    Parameters
    ----------
    color_to_words : dict(str -> list(str) | set(str))
      A dictionary that maps a color to the list/set of words.

    default_color : str
      Color that will be assigned to a word that's not a member
      of any value from color_to_words.
    """

    def __init__(
        self,
        color_to_words,  # type: Dict[str, Union[List[str], Set[str]]]
        common_words,  # type: Union[List[str], Set[str]]
        default_color=COMMON_WORDS_COLOR,  # type: str
        random_state=None,  # type: Optional[Any]
    ):
        # type: (...) -> None
        """Initialize with a list of words in common."""
        self.common_words = common_words
        self.random_state = random_state
        self.color_func_to_words = [
            (get_single_color_func(color), set(words))
            for (color, words) in color_to_words.items()
        ]

        self.common_color_func = get_single_color_func(default_color)
        self.default_color_func = get_single_color_func("yellow")

    def get_color_func(self, word):
        # type: (str) -> Callable
        """Return a single_color_func associated with the word."""
        if word in self.common_words:
            return self.common_color_func
        try:
            color_func = next(
                color_func
                for (color_func, words) in self.color_func_to_words
                if word in words
            )
        except StopIteration:  # pragma: nocover
            color_func = self.default_color_func

        return color_func

    def __call__(self, word, **kwargs):
        # type: (str, Any) -> str
        """Call the object as a function."""
        random_state = kwargs.pop("random_state", self.random_state)
        return self.get_color_func(word)(word, random_state=random_state, **kwargs)
