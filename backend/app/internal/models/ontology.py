"""-*- coding: utf-8 -*-."""
from typing import final, List, Dict, Any
from sqlalchemy import Column, Integer, String, Boolean
import pandas as pd
from .base import Base
from app.model_pb2 import Ontology as OntologyProto


@final
class Ontology(Base):
    __tablename__ = "ontology"

    id = Column(Integer, primary_key=True, name="id")
    domain = Column(String, name="domain")
    name = Column(String, name="name")
    status = Column(String, name="status")
    url = Column(String, name="url")
    active = Column(Boolean, name="active")

    def get_items_in_list(self):
        # type: () -> List[str]
        return [
            str(self.__getattribute__(key)) for key in Ontology.__table__.columns.keys()
        ]

    def ontology_to_proto(self) -> OntologyProto:
        ontology_proto = OntologyProto()
        ontology_proto.id = self.id
        ontology_proto.domain = self.domain
        ontology_proto.name = self.name
        ontology_proto.status = self.status
        ontology_proto.url = self.url
        ontology_proto.active = self.active
        return ontology_proto

    @staticmethod
    def to_dict(rows):
        header = ["id", "domain", "name", "url", "active"]
        df_dict = {}  # type: Dict[str,Any]
        for column in header:
            df_dict[column] = [
                str(getattr(row, column)) if getattr(row, column) is not None else None
                for row in rows
            ]
        return df_dict
