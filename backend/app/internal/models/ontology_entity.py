from typing import final, List, Optional, Dict, Any

from sqlalchemy import Column, String, Integer, Text, Boolean, ForeignKey
from sqlalchemy.dialects.postgresql import UUID

from .base import Base


@final
class OntologyEntity(Base):
    __tablename__ = "ontology_entity"

    id = Column(UUID, primary_key=True, name="id")  # type: UUID
    domain = Column(String, name="domain")
    ontology_class = Column(String, name="ontology_class")
    parent_class = Column(String, name="parent_class")
    description = Column(Text, name="description")
    ontology_id = Column(
        Integer,
        ForeignKey("ontology.id", ondelete="CASCADE"),
        comment="Ref: Ontology Id",
    )
    depth = Column(Integer, name="depth")
    active = Column(Boolean, name="active", default=True)
    preprocessed = Column(String, name="preprocessed", nullable=True, default=None)

    def get_items_in_list(self):
        # type: () -> List[Optional[str]]
        return [
            str(self.__getattribute__(key))
            if self.__getattribute__(key) is not None
            else None
            for key in OntologyEntity.__table__.columns.keys()
        ]

    @staticmethod
    def to_dict(rows):
        df_dict = {}  # type: Dict[str,Any]
        header = [
            "id",
            "domain",
            "ontology_class",
            "parent_class",
            "description",
            "ontology_id",
            "depth",
            "active",
            "preprocessed",
        ]
        for column in header:
            df_dict[column] = []
            if column == "depth":
                df_dict[column] = [
                    int(getattr(row, column))
                    if getattr(row, column) is not None
                    else None
                    for row in rows
                ]
            else:
                df_dict[column] = [
                    str(getattr(row, column))
                    if getattr(row, column) is not None
                    else None
                    for row in rows
                ]
        return df_dict
