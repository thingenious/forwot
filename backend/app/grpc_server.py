import os
import asyncio
from typing import Optional

import grpc
import logging
import json

import pandas

logging.basicConfig()
logging.getLogger().setLevel(
    logging.DEBUG
    if os.environ.get("APP_ENV", "production") != "production"
    else logging.INFO
)
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

try:
    import model_pb2 as pb2
    import model_pb2_grpc as pb2_grpc
    from .internal.service.ontology_service import OntologyService
    from .internal.service import WordCloudService
    from .dependencies import DATABASE_URL

except (ImportError, ModuleNotFoundError):
    import sys
    import os

    sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
    from app import model_pb2 as pb2
    from app import model_pb2_grpc as pb2_grpc
    from app.internal.service import WordCloudService
    from app.internal.service.ontology_service import OntologyService
    from app.dependencies import DATABASE_URL

_PORT = int(os.environ.get("GRPC_PORT", "8061"))

engine = create_async_engine(
    DATABASE_URL,
    echo=True,
    future=True,
)
async_session = sessionmaker(bind=engine, class_=AsyncSession, expire_on_commit=True)


class ForwotService(pb2_grpc.ForwotServicer):
    async def FetchOntologies(self, request, context):
        async with async_session() as session:
            async with session.begin():
                if request.domains:
                    ontology_list = (
                        await OntologyService.get_ontologies_by_domains_in_list(
                            session, request.domains
                        )
                    )
                    ontologies_proto = pb2.OntologyList()
                    ontologies_proto.ontology_list.extend(
                        [ontology.ontology_to_proto() for ontology in ontology_list]
                    )
                    session.close()
                    return ontologies_proto
                else:
                    ontology_list = (
                        await OntologyService.get_ontologies_by_domains_in_list(
                            session, request.domains
                        )
                    )
                    ontologies_proto = pb2.OntologyList()
                    ontologies_proto.ontology_list.extend(
                        [ontology.ontology_to_proto() for ontology in ontology_list]
                    )
                    return ontologies_proto

    async def FetchOntologiesDomains(self, request, context):
        async with async_session() as session:
            async with session.begin():
                domains = await OntologyService.get_unique_domains(session)
                domains_proto = pb2.DomainList()
                domains_proto.domains.extend(domains)
                session.close()
                return domains_proto

    async def GetWordcloud(self, request, context):
        async with async_session() as session_db:
            async with session_db.begin():
                wordcloud = await WordCloudService.create_wordcloud(
                    session_db, request.domains, depth=request.depth
                )
                wordcloud_proto = pb2.Wordcloud()
                wordcloud_proto.wordcloud = wordcloud
                return wordcloud_proto

    async def GetWordcloudStats(self, request, context):
        async with async_session() as session_db:
            async with session_db.begin():
                wordcloud_stats = await WordCloudService.get_wordcloud_stats(
                    session_db, request.domains, depth=request.depth
                )
                wordcloud_stats_proto = pb2.WordcloudStats()
                wordcloud_stats_proto.stats = json.dumps(wordcloud_stats)

                return wordcloud_stats_proto

    async def SearchSimilarity(self, request, context):
        async with async_session() as session_db:
            async with session_db.begin():
                dataframe: Optional[
                    pandas.DataFrame
                ] = await WordCloudService.get_search_similarity(
                    session_db, request.search_term, request.ontology_domain
                )
                if not dataframe.empty:
                    dataframe = dataframe.to_string()
                    dataframe_proto = pb2.SearchSimilarityDataframeToString()
                    dataframe_proto.string_dataframe = dataframe
                    return dataframe_proto
                else:
                    raise Exception("Similarity not found")

    async def SearchTermInDomain(self, request, context):
        async with async_session() as session_db:
            async with session_db.begin():
                dataframe: Optional[
                    pandas.DataFrame
                ] = await WordCloudService.search_term_in_domain(
                    session_db, request.search_term, request.ontology_domain
                )
                if not dataframe.empty:
                    dataframe = dataframe.to_string()
                    dataframe_proto = pb2.SearchSimilarityDataframeToString()
                    dataframe_proto.string_dataframe = dataframe
                    return dataframe_proto
                else:
                    raise Exception("Similarity not found")

    async def BlackListOntology(self, request, context):
        async with async_session() as session_db:
            async with session_db.begin():
                await OntologyService.blacklist_ontology(session_db, request.id)
                response_message = pb2.Message()
                response_message.message = "Ontology successfully blacklisted"
                return response_message

    async def WhiteListOntology(self, request, context):
        async with async_session() as session_db:
            async with session_db.begin():
                await OntologyService.whitelist_ontology(session_db, request.id)
                response_message = pb2.Message()
                response_message.message = "Ontology successfully whitelisted"
                return response_message


async def main() -> None:
    """Start listening."""
    server = grpc.aio.server()  # noqa
    pb2_grpc.add_ForwotServicer_to_server(ForwotService(), server)
    _SOCKET = f"[::]:{_PORT}"
    server.add_insecure_port(_SOCKET)
    await server.start()
    print(f"Server started listening on {_SOCKET}")
    await server.wait_for_termination()


if __name__ == "__main__":
    asyncio.run(main())
