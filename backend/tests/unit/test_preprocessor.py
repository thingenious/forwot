# Test functionality of DataPreprocessor class
from app.internal.service.preprocessor import Preprocessor


def test_do_upper_split(upper_split_test_cases):
    # type: (dict) -> None
    for test_case in upper_split_test_cases:
        test_result = Preprocessor.do_upper_split(test_case["test_case"])
        assert test_result == test_case["result"]


def test_join_upper(join_upper_test_cases):
    # type: (dict) -> None
    for test_case in join_upper_test_cases:
        test_result = Preprocessor.join_upper(test_case["test_case"])
        assert test_result == test_case["result"]


def test_preprocess(get_ontology_class_list):
    for test_case in get_ontology_class_list:
        test_result = Preprocessor.preprocess(test_case["test_case"])
        assert test_result == test_case["result"]
