FROM python:3.9
SHELL ["/bin/bash", "-c"]

RUN apt update && \
    apt install -y wget curl unzip cron && \
    rm -rf /var/lin/apt/lists/*

ENV PYTHONUNBUFFERED 1
ARG USER_ID=1000
ARG GROUP_ID=1000
# ports to expose
ARG REST_PORT=6080
ENV REST_PORT=$REST_PORT
ARG GRPC_PORT=8061
ENV GRPC_PORT=$GRPC_PORT

# self update to stop complaining
RUN pip install --upgrade pip

# create the user/home folder
RUN addgroup --system --gid ${GROUP_ID} runner
RUN adduser --disabled-login --disabled-password --system --uid $USER_ID --gid $GROUP_ID runner
# requirements
COPY ./requirements.txt /home/runner/requirements.txt
RUN pip install -r /home/runner/requirements.txt

# dev/test requirements (in case of dind)
ARG APP_ENV=production
ENV APP_ENV=$APP_ENV
COPY ./dev-requirements.txt /home/runner/dev-requirements.txt
COPY ./.coveragerc /home/runner/.coveragerc
COPY ./pytest.ini /home/runner/pytest.ini
RUN bash -c "if [ "$APP_ENV" = "development" ]; then pip install -r /home/runner/dev-requirements.txt; fi"

# app contents
COPY ./app /home/runner/app

# scripts
COPY ./scripts /home/runner/scripts
COPY ./forwotctl /home/runner/forwotctl
RUN chmod +x /home/runner/forwotctl

# cron
RUN echo "0 0 * * *  /home/runner/forwotctl --ontologies > /proc/1/fd/1 2>&1" >> /etc/cron.d/ontologies && \
    crontab -u runner /etc/cron.d/ontologies && \
    chmod u+s /usr/sbin/cron

# to use embeddings as volume (persistence)
RUN mkdir -p /home/runner/embeddings

RUN chown -R runner /home/runner
RUN chgrp -R runner /home/runner

# do not run as root
USER runner
WORKDIR /home/runner

EXPOSE $GRPC_PORT $REST_PORT

CMD ["/home/runner/forwotctl", "--start"]
